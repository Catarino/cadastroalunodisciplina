package CadastroAlunoDisciplina;

import java.util.ArrayList;
public class ControleAluno{
	
	//Atributos
	private ArrayList<Aluno> listaAluno;
	
	//Construtor
	public ControleAluno(){
		listaAluno = new ArrayList<Aluno>();
	}

	//Métodos
	public void adicionarAluno(Aluno umAluno){
		listaAluno.add(umAluno);
	}
	public void removerAluno(Aluno umAluno){
		listaAluno.remove(umAluno);
	}
	public Aluno buscar(String umNome){
		for(Aluno umAluno : listaAluno){
			if(umAluno.getNome().equalsIgnoreCase(umNome))
				return umAluno;
		}
		return null;
	}
	public void mostrarAlunos() {
		for (Aluno umAluno : listaAluno) {
			System.out.println ("Nome: " + umAluno.getNome() + " Matricula: " + umAluno.getMatricula());
		}
	}	
}
