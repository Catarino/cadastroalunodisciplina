package CadastroAlunoDisciplina;

import java.io.*;
public class CadastroAlunoDisciplina{
	


	public static void main(String [] args) throws IOException{
		int menuOpcoes;
		
		InputStream entradaSistema=System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada=new BufferedReader(leitor);
		String entradaTeclado;
		
		//Instanciando as classes ControleAluno e ControleDisciplina
		ControleAluno umControleAluno = new ControleAluno();
		ControleDisciplina umControleDisciplina = new ControleDisciplina();
		
		do{
		    System.out.println("==========  Menu   ==========");
		    System.out.println("1) Cadastrar aluno");
		    System.out.println("2) Cadastrar disciplina");
		    System.out.println("3) Remover aluno");
		    System.out.println("4) Remover disciplina");
		    System.out.println("5) Cadastrar aluno na disciplina");
		    System.out.println("6) Mostrar Aluno");
		    System.out.println("7) Mostrar Disciplina");
		    System.out.println("8) Mostrar Alunos da Disciplina");
		    System.out.println("9)Sair");
		    System.out.println("Escolha a opcao");
		    System.out.println("================================");
		    entradaTeclado = leitorEntrada.readLine();
		    menuOpcoes = Integer.parseInt(entradaTeclado);
		    
		    switch(menuOpcoes){
		        case 1:
		        	System.out.println("========  Cadastro Aluno  ========");
		            System.out.println("Nome: ");
		            entradaTeclado = leitorEntrada.readLine();
		            String umNome = entradaTeclado;
		            
		            System.out.println("Matricula: ");
		            entradaTeclado = leitorEntrada.readLine();
		            String umaMatricula = entradaTeclado;
		            
		            Aluno umAluno = new Aluno(umNome,umaMatricula);
		            umControleAluno.adicionarAluno(umAluno);
		            break;
	            case 2:
		        	System.out.println("========  Cadastro Disciplina  ========");
		            System.out.println("Nome da Disciplina: ");
		            entradaTeclado = leitorEntrada.readLine();
		            umNome = entradaTeclado;
		            
		            System.out.println("Codigo da Disciplina: ");
		            entradaTeclado = leitorEntrada.readLine();
		            int umCodigo = Integer.parseInt(entradaTeclado);
		            
		            System.out.println("Digite o Hor�rio: ");
		            entradaTeclado = leitorEntrada.readLine();
		            String umHorario = entradaTeclado;
		            
		            Disciplina umaDisciplina = new Disciplina(umNome, umHorario, umCodigo);
		            umControleDisciplina.adicionarDisciplina(umaDisciplina);
		            break;
	            case 3:
	            	System.out.println("Digite o nome do Aluno: ");
	            	entradaTeclado = leitorEntrada.readLine();
	            	umNome = entradaTeclado;
	            	umAluno = umControleAluno.buscar(umNome);
	            	if(umAluno != null){
	            		umControleAluno.removerAluno(umAluno);
	            	}
	            	else
	            		System.out.println("Aluno n�o encontrado.");            	
	            	break;
	            case 4:
	            	System.out.println("Digite o nome da Mat�ria: ");
	            	entradaTeclado = leitorEntrada.readLine();
	            	umNome = entradaTeclado;
	            	umaDisciplina = umControleDisciplina.buscarDisciplina(umNome);
	            	if(umaDisciplina != null){
	            		umControleDisciplina.removerDisciplina(umaDisciplina);
	            	}
	            	else
	            		System.out.println("Disciplina n�o encontrado.");            	
	            	break;
	            case 5:
	            	System.out.println("========  Cadastrando Aluno na Disciplina  ========");
		            System.out.println("Nome: ");
		            entradaTeclado = leitorEntrada.readLine();
		            umNome = entradaTeclado;
		            
		            umAluno = umControleAluno.buscar(umNome);
		            
		            System.out.println("Digite o nome da Disciplina: ");
		            entradaTeclado = leitorEntrada.readLine();
		            String umNomeDisciplina = entradaTeclado;		            
		            umaDisciplina = umControleDisciplina.buscarDisciplina(umNomeDisciplina);
		            if(umaDisciplina != null){
		            	umaDisciplina.adicionarAluno(umAluno,umaDisciplina);
		            }
		            else
		            	System.out.println("Disciplina n�o encontrado.");            	
	            	break;
	            case 6:
	            	umControleAluno.mostrarAlunos();
	            	break;
	            case 7:
	            	umControleDisciplina.mostrarDisciplina();
	            	break;
	            case 8:
	            	System.out.println("Digite o nome da Mat�ria: ");
	            	entradaTeclado = leitorEntrada.readLine();
	            	umNome = entradaTeclado;
	            	umaDisciplina = umControleDisciplina.buscarDisciplina(umNome);
	            	if(umaDisciplina != null){
	            		umaDisciplina.mostrarAluno(umaDisciplina);
	            	}
	            	else
	            		System.out.println("Disciplina n�o encontrado.");     
	            break;
		    }
		}while(menuOpcoes != 9);
		System.out.println("	========   END   ========");


	}
	
}
