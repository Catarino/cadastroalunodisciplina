package CadastroAlunoDisciplina;

import java.util.ArrayList;
public class ControleDisciplina{
	
	//Atributos
	private ArrayList<Disciplina> listaDisciplina;
	
	//Construtor
	public ControleDisciplina(){
		listaDisciplina = new ArrayList<Disciplina>();
	}
	
	//Métodos

	public void adicionarDisciplina(Disciplina umaDisciplina){
		listaDisciplina.add(umaDisciplina);
	}
	public void removerDisciplina(Disciplina umaDisciplina){
		listaDisciplina.remove(umaDisciplina);
	}
	public Disciplina buscarDisciplina(String umNome){
		for(Disciplina umaDisciplina : listaDisciplina){
			if(umaDisciplina.getNome().equalsIgnoreCase(umNome))
				return umaDisciplina;
		}
		return null;
	}
	public void mostrarDisciplina(){
		for(Disciplina umaDisciplina : listaDisciplina){
			System.out.println("---------------------------------------------");
			System.out.println("Nome: "+umaDisciplina.getNome());
			System.out.println("Codigo: "+umaDisciplina.getCodigo());
			System.out.println("Horario: "+umaDisciplina.getHorario());
			System.out.println("---------------------------------------------");
		}
	}

}
