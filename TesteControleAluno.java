package CadastroAlunoDisciplina;

import static org.junit.Assert.*;

import org.junit.Test;

public class TesteControleAluno {
	ControleAluno umControleAluno;
	Aluno novoAluno;
	
	@Test
	public void testeAdicionarAluno() {
		novoAluno=new Aluno();
		novoAluno.setNome("Fulano");
		umControleAluno=new ControleAluno();
		umControleAluno.adicionarAluno(novoAluno);
		assertEquals("Fulano", umControleAluno.adicionarAluno(novoAluno));
	
	}
	
	@Test
	public void testeBuscar(){
		Aluno umNome=new Aluno();
		umNome.setNome("Fulano");
		umNome.setMatricula("123");
		umControleAluno=new ControleAluno();
		assertEquals(("Fulano"), umControleAluno.buscar("Fulano"));
	}
}
