package CadastroAlunoDisciplina;

import java.util.ArrayList;
public class Disciplina{
	
	//atributos
	private ArrayList<Aluno> listaAlunoDisciplina;
	private String nome;
	private String horario;
	private int codigo;

	//construtor
	
		
	
	public Disciplina (String umNome, int umCodigo){
		nome = umNome;
		codigo = umCodigo;
		listaAlunoDisciplina = new ArrayList<Aluno>();
	}
	public Disciplina(String umNome){
		nome = umNome;
	}
	public Disciplina (String umNome, String umHorario, int umCodigo){
		nome= umNome;
		horario = umHorario;
		codigo = umCodigo;
	}

	public void setNome(String umNome){
		nome=umNome;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setHorario(String umHorario){
		horario = umHorario;
	}

	public String getHorario(){
		return horario;
	}
	
	public void setCodigo (int umCodigo){
		codigo = umCodigo;
	}
	public void adicionarAluno(Aluno umAluno,Disciplina umaDisciplina) {
		listaAlunoDisciplina.add(umAluno);
	}
	
	public void remover (Aluno umAluno) {
		listaAlunoDisciplina.remove(umAluno);
	}
	
	public int getCodigo(){
		return codigo;
	}
	public void mostrarAluno(Disciplina umaDisciplina){
		if(listaAlunoDisciplina != null)
		{
			for(Aluno umAluno : listaAlunoDisciplina){
				System.out.println("Nome: "+umAluno.getNome() +"Matricula: "+umAluno.getMatricula());
			}
			System.out.println("Essa mat�ria n�o possui nenhum aluno cadastrado ainda.");
		}
	}
}
