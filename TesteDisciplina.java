package CadastroAlunoDisciplina;

import static org.junit.Assert.*;

import org.junit.Test;

public class TesteDisciplina {
	@Test
	public void testNome() {
		Disciplina novaDisciplina = new Disciplina(null);
		novaDisciplina.setNome("OO");
		assertEquals("OO", novaDisciplina.getNome());
	}
	
	@Test
	public void testCodigo() {
		Disciplina novaDisciplina = new Disciplina(null);
		novaDisciplina.setCodigo(12345);
		assertEquals(12345, novaDisciplina.getCodigo());
	}
	
	@Test
	public void testHorario() {
		Disciplina novaDisciplina = new Disciplina(null);
		novaDisciplina.setHorario("10:00");
		assertEquals("10:00", novaDisciplina.getHorario());
	}
	

}
